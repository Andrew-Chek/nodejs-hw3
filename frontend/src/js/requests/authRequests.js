const { viewLoads } = require("./loadRequests");
const {getUserRequest} = require("./requests")

async function loginRequest()
{
    const header = document.querySelector(".header");
    const article = document.querySelector(".main__page");
    const authDiv = document.querySelector(".content__auth");
    const emailLabel = document.querySelector(".profile__label");
    const emailInput = document.querySelector(".form__old .checked_items");
    const pswInput = document.querySelector(".form__new .checked_items");

    const profile = document.querySelector(".result__profile");
    const trucks = document.querySelector(".result__trucks");
    const loads = document.querySelector(".result__loads");

    const email = emailInput.value;
    const password = pswInput.value;

    const res = await fetch('http://localhost:8080/api/auth/login', {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email, password}),
        mode: 'cors'
    })
    const text = await res.json();

    if(res.status == 200)
    {
        emailLabel.innerText = `Welcome ${email}!`;

        const myStorage = window.localStorage;
        myStorage.setItem('jwt', text.jwt_token);

        header.style.display = 'block'
        article.style.display = 'grid'
        authDiv.style.display = 'none'
        profile.style.display = 'block'
        trucks.style.display = 'none'
        loads.style.display = 'none'

        viewLoads();
        const user = await getUserRequest();
        const img = document.querySelector('.result__profile .img');
        img.setAttribute('src', user.user.img_src);
        const truckMenu = document.querySelector('.trucks__li')
        const deleteBtn = document.querySelector('.delete__load');
        const postBtn = document.querySelector('.post');
        const addBtn = document.querySelector('.add__load');
        const nextBtn = document.querySelector('.iterate__next');
        const getActiveBtn = document.querySelector('.active')
        const statusOption = document.querySelector('.for__loads')
        myStorage.setItem('role', user.user.role);
        if(user.user.role == 'SHIPPER')
        {
            truckMenu.style.display = 'none'
            deleteBtn.style.display = 'block'
            postBtn.style.display = 'block'
            getActiveBtn.style.display = 'none'
            addBtn.style.display = 'block'
            nextBtn.style.display = 'none'
            statusOption.style.display = 'block'
        }
        else
        {
            truckMenu.style.display = 'list-item'
            getActiveBtn.style.display = 'block'
            deleteBtn.style.display = 'none'
            postBtn.style.display = 'none'
            addBtn.style.display = 'none'
            nextBtn.style.display = 'block'
            statusOption.style.display = 'none'
        }
    }
    else
    {
        const response = document.querySelector(".response__text")
        response.innerText = `message: ${text.message}`;
    }

    const btn = document.querySelector("#popup2 .form__btn");
    btn.removeEventListener('click', loginRequest)
}

async function registerRequest()
{
    const emailInput = document.querySelector(".form__email .checked_items");
    const pswInput = document.querySelector(".form__password .checked_items");
    const roleValue = document.querySelector("#form__role");

    const email = emailInput.value;
    const password = pswInput.value;
    const role = roleValue.value;

    const res = await fetch('http://localhost:8080/api/auth/register', {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email, password, role}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;

    const btn = document.querySelector(".form__submit .form__btn");
    btn.removeEventListener('click', registerRequest)
}

async function forgotPasswordRequest()
{
    const emailInput = document.querySelector(".form__fgtpsw .checked_items");
    const email = emailInput.value;

    const res = await fetch('http://localhost:8080/api/auth/forgot_password', {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;

    const btn = document.querySelector(".form__submit .form__btn");
    btn.removeEventListener('click', forgotPasswordRequest)
}

module.exports = {
    loginRequest, 
    registerRequest, 
    forgotPasswordRequest
}