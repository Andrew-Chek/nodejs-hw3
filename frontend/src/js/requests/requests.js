async function changePasswordRequest()
{
    const nameInput = document.querySelector(".form__old .checked_items");
    const pswInput = document.querySelector(".form__new .checked_items");
    const oldPassword = nameInput.value;
    const newPassword = pswInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me/password', {
        method: 'PATCH', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({oldPassword, newPassword}),
        mode: 'cors'
    })
    const text = await res.json();
    const response = document.querySelector(".response__text")
    response.innerText = `message: ${text.message}`;
    const btn = document.querySelector("#popup2 .form__btn");
    btn.removeEventListener('click', changePasswordRequest)
}

async function getUserRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me', {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const text = await res.json();
    return text;
}

async function deleteUserRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me', {
        method: 'DELETE', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;
}

async function postPhotoRequest(base64String)
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me/photo', {
        method: 'POST', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'image/jpeg'
        },
        body: {data: base64String},
        mode: 'cors'
    })
    const parsedResult = await res.json();
    if(parsedResult.status == 200)
    {
        getPhotoRequest()
    }
}

async function getPhotoRequest()
{
    const user = await getUserRequest();
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const src = `./files/${user.user.email.jpg}`

    const res = await fetch('http://localhost:8080/api/users/me/photo', {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: {src},
        mode: 'cors'
    })
    const text = await res.json();
    //TO DO WRITE FILE TO JPG
    const newHandle = await window.showSaveFilePicker();
    const writableStream = await newHandle.createWritable();
    await writableStream.write(text.text);
}

module.exports = {
    changePasswordRequest,
    getUserRequest,
    deleteUserRequest,
    postPhotoRequest
}