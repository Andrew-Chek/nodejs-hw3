const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime')

async function getUserTrucksRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/trucks', {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const text = await res.json();
    return text;
}

async function createTruckRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const typeInput = document.querySelector("#form__type");
    const type = typeInput.value;

    const res = await fetch('http://localhost:8080/api/trucks', {
        method: 'POST', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {
                type
            }),
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup4 .form__submit2");
    btn.removeEventListener('click', createTruckRequest);
    viewTrucks();
    return result;
}

async function editTruckRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')
    const id = myStorage.getItem('truck_id');

    const typeInput = document.querySelector("#form__type");
    const type = typeInput.value;

    const res = await fetch(`http://localhost:8080/api/trucks/${id}`, {
        method: 'PUT', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {
                type
            }),
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup4 .form__btn");
    btn.removeEventListener('click', editTruckRequest);
    viewTrucks();

    return result;
}

async function deleteTruckRequest()
{
    const idInput = document.querySelector("#result__truck_id");
    const id = idInput.innerText;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/trucks/${id}`, {
        method: 'DELETE', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup7 .delete__truck");
    btn.removeEventListener('click', deleteTruckRequest);
    viewTrucks();

    return result;
}

async function assignTruckRequest()
{
    const idInput = document.querySelector("#result__truck_id");
    const id = idInput.innerText;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/trucks/${id}/assign`, {
        method: 'POST', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup7 .assign");
    btn.removeEventListener('click', assignTruckRequest);
    viewTrucks();

    return result;
}

async function viewTrucks()
{
    const profile = document.querySelector(".result__profile");
    const trucks = document.querySelector(".result__trucks");
    const loads = document.querySelector(".result__loads");

    profile.style.display = 'none';
    trucks.style.display = 'block';
    loads.style.display = 'none';

    const resTrucks = await getUserTrucksRequest();
    addTruckRows(resTrucks.trucks)
}

function createTruckField(truck)
{
    dayjs.extend(relativeTime)
    const table = document.querySelector('.trucks__body');

    const row = document.createElement('tr');
    const tdType = document.createElement('td');
    const tdDate = document.createElement('td');
    const tdStatus = document.createElement('td');
    const tdGet = document.createElement('td');
    const tdEdit = document.createElement('td');

    row.setAttribute('class', 'load__tr');
    tdGet.setAttribute('class', 'interact');
    tdEdit.setAttribute('class', 'interact');
    tdGet.innerHTML = '<a class="popup_btn popup7__btn" href="#popup7"><i class="fa fa-eye" aria-hidden="true"></i></a>'
    tdEdit.innerHTML = '<a class="popup_btn popup4__btn" href="#popup4"><i class="fa fa-pencil" aria-hidden="true"></i></a>';

    tdGet.addEventListener('click', function() 
    {
        dayjs.extend(relativeTime)
        const id = document.querySelector("#result__truck_id");
        const createdBy = document.querySelector("#result__truck_createdby");
        const assignedTo = document.querySelector("#result__truck_assignedto");
        const type = document.querySelector("#result__truck_type");
        const status = document.querySelector("#result__truck_status");
        const date = document.querySelector("#result__truck_date");
        const deleteBtn = document.querySelector("#popup7 .delete__truck");
        const assignBtn = document.querySelector(".view__truck .assign");

        id.innerText = truck._id;
        createdBy.innerText = truck.created_by;
        assignedTo.innerText = truck.assigned_to;
        type.innerText = truck.type;
        status.innerText = truck.status;
        date.innerText = dayjs(truck.created_date).fromNow();

        const myStorage = window.localStorage;
        myStorage.setItem('truck_id', truck._id);

        deleteBtn.addEventListener('click', deleteTruckRequest);
        assignBtn.addEventListener('click', assignTruckRequest);
    })

    tdEdit.addEventListener('click', function()
    {
        const upLabel = document.querySelector("#popup4 .up__label");
        const type = document.querySelector("#form__type");
        const submit = document.querySelector("#popup4 .form__submit .form__btn");

        type.value = truck.type;

        const myStorage = window.localStorage;
        myStorage.setItem('truck_id', truck._id);
        
        upLabel.innerText = "Edit truck";
        submit.innerText = 'Edit truck';
        submit.addEventListener('click', editTruckRequest)
    })

    tdType.innerText = truck.type;
    tdDate.innerText = dayjs(truck.created_date).fromNow();
    tdStatus.innerText = truck.status;
    
    row.appendChild(tdType);
    row.appendChild(tdDate);
    row.appendChild(tdStatus);
    row.appendChild(tdGet);
    row.appendChild(tdEdit);
    table.appendChild(row);
}

function addTruckRows(trucks)
{
    const table = document.querySelector('.trucks__body');
    table.innerHTML = '';
    
    for(let truck of trucks)
    {
        createTruckField(truck);
    }
}

module.exports = {
    viewTrucks,
    createTruckRequest
}