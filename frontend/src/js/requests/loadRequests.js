const {checkInputsError} = require('./validation')
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime')

async function getUserLoadsRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')
    const statusInput = document.querySelector('#status')

    const res = await fetch(`http://localhost:8080/api/loads?status=${statusInput.value}`, {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const text = await res.json();
    return text;
}

async function getLoadPage(pageNum)
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')
    const statusInput = document.querySelector('#status')

    const res = await fetch(`http://localhost:8080/api/loads?offset=${pageNum*5}&limit=5&status=${statusInput.value}`, {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const text = await res.json();
    return text;
}

async function createLoadRequest()
{
    const btn = document.querySelector("#popup5 .form__submit2");
    const errorDetected = checkInputsError();
    if(errorDetected)
    {
        btn.setAttribute('href', '#popup5');
        return;
    }
    else
    {
        const myStorage = window.localStorage;
        const jwt = myStorage.getItem('jwt')

        const nameInput = document.querySelector("#form__load_name");
        const payloadInput = document.querySelector("#form__payload");
        const pickupInput = document.querySelector("#form__pickup");
        const deliveryInput = document.querySelector("#form__delivery");
        const widthInput = document.querySelector("#form__width");
        const lengthInput = document.querySelector("#form__lgh");
        const heightInput = document.querySelector("#form__height");

        const name = nameInput.value;
        const payload = payloadInput.value;
        const pickup_address = pickupInput.value;
        const delivery_address = deliveryInput.value;
        const width = widthInput.value;
        const length = lengthInput.value;
        const height = heightInput.value;

        const res = await fetch('http://localhost:8080/api/loads', {
            method: 'POST', 
            headers: {
                'Authorization': `Bearer ${jwt}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(
                {
                    name, 
                    payload, 
                    pickup_address, 
                    delivery_address,
                    dimensions: {
                        width,
                        length,
                        height
                    }
                }),
            mode: 'cors'
        })
        const result = await res.json();

        btn.removeEventListener('click', createLoadRequest);
        btn.setAttribute('href', '#');
        viewLoads();
        removeAllSpans();
        return result;
    }
}

async function viewLoads()
{
    const profile = document.querySelector(".result__profile");
    const trucks = document.querySelector(".result__trucks");
    const loads = document.querySelector(".result__loads");

    profile.style.display = 'none';
    trucks.style.display = 'none';
    loads.style.display = 'block';

    const resLoads = await getLoadPage(0);
    addAllRows(resLoads.loads)
}

async function createLoadField(load)
{
    dayjs.extend(relativeTime)
    const table = document.getElementById('table_body');

    const row = document.createElement('tr');
    const tdName = document.createElement('td');
    const tdDate = document.createElement('td');
    const tdPickup = document.createElement('td');
    const tdDelivery = document.createElement('td');
    const tdGet = document.createElement('td');
    const tdEdit = document.createElement('td');

    row.setAttribute('class', 'load__tr');
    tdGet.setAttribute('class', 'interact');
    tdEdit.setAttribute('class', 'interact');
    tdGet.innerHTML = '<a class="popup_btn popup6__btn" href="#popup6"><i class="fa fa-eye" aria-hidden="true"></i></a>'
    tdEdit.innerHTML = '<a class="popup_btn popup5__btn" href="#popup5"><i class="fa fa-pencil" aria-hidden="true"></i></a>';

    tdGet.addEventListener('click', function() 
    {
        dayjs.extend(relativeTime)
        const id = document.querySelector("#result__id");
        const createdBy = document.querySelector("#result__createdby");
        const assignedTo = document.querySelector("#result__assignedto");
        const status = document.querySelector("#result__status");
        const state = document.querySelector("#result__state");
        const name = document.querySelector("#result__name");
        const payload = document.querySelector("#result__payload");
        const pickup = document.querySelector("#result__pickup");
        const delivery = document.querySelector("#result__delivery");
        const width = document.querySelector("#result__width");
        const length = document.querySelector("#result__length");
        const height = document.querySelector("#result__height");
        const date = document.querySelector("#result__date");
        const deleteBtn = document.querySelector("#popup6 .delete__load");
        const postBtn = document.querySelector("#popup6 .post");

        id.innerText = load._id;
        createdBy.innerText = load.created_by;
        assignedTo.innerText = load.assigned_to;
        status.innerText = load.status;
        state.innerText = load.state;
        name.innerText = load.name;
        payload.innerText = load.payload;
        pickup.innerText = load.pickup_address;
        delivery.innerText = load.delivery_address;
        width.innerText = load.dimensions.width;
        length.innerText = load.dimensions.length;
        height.innerText = load.dimensions.height;
        date.innerText = dayjs(load.created_date).fromNow();
        const myStorage = window.localStorage;
        myStorage.setItem('load_id', load._id);

        deleteBtn.addEventListener('click', deleteLoadRequest);
        postBtn.addEventListener('click', postLoadRequest);
        if(load.status == 'SHIPPED')
        {
            deleteBtn.style.display = 'block'
            postBtn.style.display = 'none'
        }
        else if(load.status != 'SHIPPED' && load.status != 'ASSIGNED')
        {
            deleteBtn.style.display = 'block'
            postBtn.style.display = 'block'
        }
        else if(load.status == 'ASSIGNED')
        {
            deleteBtn.style.display = 'none'
            postBtn.style.display = 'none'
        }
        if(myStorage.getItem('role') == 'DRIVER')
        {
            deleteBtn.style.display = 'none'
            postBtn.style.display = 'none'
        }

        const nextBtn = document.querySelector('.iterate__next');
        nextBtn.style.display = 'none'
    })

    tdEdit.addEventListener('click', function()
    {
        const upLabel = document.querySelector("#popup5 .up__label");
        const name = document.querySelector("#form__load_name");
        const payload = document.querySelector("#form__payload");
        const pickup = document.querySelector("#form__pickup");
        const delivery = document.querySelector("#form__delivery");
        const width = document.querySelector("#form__width");
        const length = document.querySelector("#form__lgh");
        const height = document.querySelector("#form__height");
        const submit = document.querySelector("#popup5 .form__submit .form__btn");

        name.value = load.name;
        payload.value = load.payload;
        pickup.value = load.pickup_address;
        delivery.value = load.delivery_address;
        width.value = load.dimensions.width;
        length.value = load.dimensions.length;
        height.value = load.dimensions.height;
        const myStorage = window.localStorage;
        myStorage.setItem('load_id', load._id);
        
        upLabel.innerText = "Edit load";
        submit.innerText = 'Edit load';
        submit.addEventListener('click', editLoadRequest)
    })

    tdName.innerText = load.name;
    tdDate.innerText = dayjs(load.created_date).fromNow();
    tdPickup.innerText = load.pickup_address;
    tdDelivery.innerText = load.delivery_address;
    
    row.appendChild(tdName);
    row.appendChild(tdDate);
    row.appendChild(tdPickup);
    row.appendChild(tdDelivery);
    row.appendChild(tdGet);
    row.appendChild(tdEdit);
    table.appendChild(row);
    const myStorage = window.localStorage;
    if(myStorage.getItem('role') == 'DRIVER')
    {
        tdEdit.style.display = 'none'
    }
    else
    {
        tdEdit.style.display = 'block'
    }
}

function addAllRows(loads)
{
    const table = document.getElementById('table_body');
    table.innerHTML = '';
    
    for(let load of loads)
    {
        createLoadField(load);
    }
}

async function editLoadRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')
    const id = myStorage.getItem('load_id');

    const nameInput = document.querySelector("#form__load_name");
    const payloadInput = document.querySelector("#form__payload");
    const pickupInput = document.querySelector("#form__pickup");
    const deliveryInput = document.querySelector("#form__delivery");
    const widthInput = document.querySelector("#form__width");
    const lengthInput = document.querySelector("#form__lgh");
    const heightInput = document.querySelector("#form__height");

    const name = nameInput.value;
    const payload = payloadInput.value;
    const pickup_address = pickupInput.value;
    const delivery_address = deliveryInput.value;
    const width = widthInput.value;
    const length = lengthInput.value;
    const height = heightInput.value;

    const res = await fetch(`http://localhost:8080/api/loads/${id}`, {
        method: 'PUT', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {
                name, 
                payload, 
                pickup_address, 
                delivery_address,
                dimensions: {
                    width,
                    length,
                    height
                }
            }),
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup5 .form__btn");
    btn.removeEventListener('click', editLoadRequest)
    viewLoads();

    return result;
}

async function deleteLoadRequest()
{
    const idInput = document.querySelector("#result__id");
    const id = idInput.innerText;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/loads/${id}`, {
        method: 'DELETE', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup6 .delete__load");
    btn.removeEventListener('click', deleteLoadRequest);
    viewLoads();

    return result;
}

async function postLoadRequest()
{
    const idInput = document.querySelector("#result__id");
    const id = idInput.innerText;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/loads/${id}/post`, {
        method: 'POST', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const result = await res.json();

    const btn = document.querySelector("#popup6 .post");
    btn.removeEventListener('click', postLoadRequest);
    viewLoads();

    return result;
}

async function getActiveRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/loads/active`, {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })

    const result = await res.json();
    viewLoads()
    if(res.status == 200)
    {
        return result;
    }
    else
    {
        return false;
    }
}

async function iterateNextRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/loads/active/state`, {
        method: 'PATCH', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })

    const result = await res.json();
    const nextBtn = document.querySelector('.iterate__next');
    nextBtn.removeEventListener('click', iterateNextRequest)
    return result;
}

function removeAllSpans()
{
    const spans = document.querySelectorAll('.error_span');
    spans.forEach( (span) => {
        span.remove();
    });
}

module.exports = {
    getUserLoadsRequest,
    getLoadPage,
    createLoadRequest,
    editLoadRequest,
    getActiveRequest,
    iterateNextRequest,
    viewLoads,
    addAllRows,
    removeAllSpans
}