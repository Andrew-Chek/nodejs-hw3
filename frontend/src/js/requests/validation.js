function checkInputsError()
{
    const checkedElements = document.querySelectorAll('#popup5 .checked_items');
    let errorDetected = false;
    for(let elem of checkedElements)
    {
        if(elem.value === '')
        {
            setSpan(elem.parentElement, 'This field is require to be filled');
            errorDetected = true;
        }
    }
    const integers = document.getElementsByClassName('integer');
    for(let integer of integers)
    {
        if(isNaN(parseInt(integer.value)))
        {
            setSpan(integer.parentElement, 'This field is require to be integer');
            errorDetected = true;
        }
    }
    return errorDetected;
}

function setSpan(parent, message)
{
    const span = document.createElement('span');
    span.setAttribute('class', 'error_span');
    span.innerText = message;

    parent.appendChild(span);
}

module.exports = {
    checkInputsError
}