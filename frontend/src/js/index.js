const {
    addDataPopupEvent, 
    addRequestEvents, 
    addLogoutEvent, 
    addCloseMenuEvent} 
        = require('./events/requestEvents.js');
const {
    addViewProfileEvent, 
    addDeleteAccountEvent, 
    addViewLoadsEvent,
    addViewTrucksEvent}
        = require('./events/fillResultBlockEvents')
const{addPaginationEvents, addFilterEvent} = require('./events/paginationEvents')
const {addPhotoEvent} = require('./events/addPhoto')

const btns = document.querySelectorAll('.popup_btn');
btns.forEach(btn => {
    addDataPopupEvent(btn);
})

addCloseMenuEvent();
addLogoutEvent();
addRequestEvents();

addViewProfileEvent();
addDeleteAccountEvent();
addViewLoadsEvent();
addViewTrucksEvent();
addPaginationEvents();
addFilterEvent();
addPhotoEvent();