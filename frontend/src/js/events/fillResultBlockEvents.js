const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime')

const {getUserRequest, 
    deleteUserRequest} = require('../requests/requests.js');
const {logout} = require('./requestEvents')
const {viewLoads} = require('../requests/loadRequests')
const {viewTrucks} = require('../requests/truckRequests')

async function viewProfile()
{
    const profile = document.querySelector(".result__profile");
    const trucks = document.querySelector(".result__trucks");
    const loads = document.querySelector(".result__loads");

    profile.style.display = 'grid';
    trucks.style.display = 'none';
    loads.style.display = 'none';
    
    const text = await getUserRequest();
    dayjs.extend(relativeTime)

    const email = document.querySelector(".email__pgh");
    const role = document.querySelector(".role__pgh");
    const date = document.querySelector(".profile__date");

    email.innerText = text.user.email;
    role.innerText = text.user.role;
    date.innerText = dayjs(text.user.created_date).fromNow()
}

function addViewProfileEvent()
{
    const btn = document.querySelector(".profile__li");
    btn.addEventListener('click', viewProfile)
}

function addDeleteAccountEvent()
{
    const btn = document.querySelector(".delete");
    btn.addEventListener('click', deleteUserRequest);
    btn.addEventListener('click', logout);
}

function addViewLoadsEvent()
{
    const btn = document.querySelector(".new__loads");
    btn.addEventListener('click', viewLoads)
}

function addViewTrucksEvent()
{
    const btn = document.querySelector(".trucks__li");
    btn.addEventListener('click', viewTrucks)
}

module.exports = {
    addViewProfileEvent,
    addDeleteAccountEvent,
    addViewLoadsEvent,
    addViewTrucksEvent
}