const { postPhotoRequest } = require('../requests/requests');


async function addPhotoEvent()
{
    const photoDiv = document.querySelector('.img__file');
    photoDiv.addEventListener('change', async function()
    {
        console.log(photoDiv.files[0])
        readFile(photoDiv.files[0])
    })
}

function readFile(file) {
    let myReader = new FileReader();
    myReader.onloadend = function (e) {
        cb(myReader.result);
    };
    myReader.readAsDataURL(file);
}

function cb(base64string) {
    postPhotoRequest(base64string)
}

module.exports = {
    addPhotoEvent
}