const {changePasswordRequest, getUserRequest, 
        deleteUserRequest} = require('../requests/requests.js');
const {loginRequest, registerRequest, forgotPasswordRequest } = require('../requests/authRequests');
const {createLoadRequest, editLoadRequest, getActiveRequest, iterateNextRequest} = require('../requests/loadRequests')
const {createTruckRequest} = require('../requests/truckRequests')
const {removeAllSpans} = require('../requests/loadRequests')
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime')

const funcDictionary = {}

function fillDictionary()
{
    funcDictionary['Login'] = loginRequest;
    funcDictionary['Register'] = registerRequest;
    funcDictionary['Forgot password'] = forgotPasswordRequest;

    funcDictionary['Change password'] = changePasswordRequest;
    funcDictionary['Get user info'] = getUserRequest;
    funcDictionary['Delete user info'] = deleteUserRequest;

    funcDictionary['Add load'] = createLoadRequest;
    funcDictionary['Edit load'] = editLoadRequest;
    funcDictionary['Add truck'] = createTruckRequest;
    funcDictionary['View active'] = getActiveRequest;
}

function addDataPopupEvent(btn)
{
    fillDictionary();
    btn.addEventListener('click', function(){
        if(btn.classList.contains('popup1__btn'))
        {
            setPopup1(btn);
        }
        else if(btn.classList.contains('popup2__btn'))
        {
            setPopup2(btn);
        }
        else if(btn.classList.contains('popup3__btn'))
        {
            setPopup3(btn);
        }
        else if(btn.classList.contains('popup4__btn'))
        {
            setPopup4(btn);
        }
        else if(btn.classList.contains('popup5__btn'))
        {
            setPopup5(btn);
        }
        else if(btn.classList.contains('popup6__btn'))
        {
            setPopup6(btn)
        }
    });
}

function setPopup1(btn)
{
    const emailInput = document.querySelector(".form__email .checked_items");
    const pswInput = document.querySelector(".form__password .checked_items");
    const submit = document.querySelector(".form__submit .form__btn");

    emailInput.value = "";
    pswInput.value = "";
    submit.addEventListener('click', funcDictionary[btn.innerText]);
}

function setPopup2(btn)
{
    const upLabel = document.querySelector("#popup2 .up__label");
    const firstFieldLabel = document.querySelector(".form__old .form__label");
    const secondFieldLabel = document.querySelector(".form__new .form__label");
    const oldPsw = document.querySelector(".form__old .checked_items");
    const newPsw = document.querySelector(".form__new .checked_items");
    const submit = document.querySelector("#popup2 .form__submit .form__btn");

    oldPsw.value = "";
    newPsw.value = "";
    upLabel.innerText = btn.innerText;
    submit.innerText = btn.innerText;

    if(btn.innerText == 'Login')
    {
        firstFieldLabel.innerText = "Email";
        oldPsw.setAttribute('placeholder', 'Enter email')
        secondFieldLabel.innerText = "Password";
        newPsw.setAttribute('placeholder', 'Enter password')
    }
    else
    {
        firstFieldLabel.innerText = "Old password";
        oldPsw.setAttribute('placeholder', 'Enter old password')
        secondFieldLabel.innerText = "New password";
        newPsw.setAttribute('placeholder', 'Enter new password')
    }
    submit.addEventListener('click', funcDictionary[btn.innerText])
    if(btn.innerText == 'Change password')
    {
        submit.addEventListener('click', logout)
    }
}

function setPopup3(btn)
{
    const nameInput = document.querySelector(".form__fgtpsw .checked_items");
    const submit = document.querySelector("#popup3 .form__submit .form__btn");

    nameInput.value = "";
    submit.addEventListener('click', funcDictionary[btn.innerText])
}

function setPopup4(btn)
{
    const upLabel = document.querySelector("#popup4 .up__label");
    const type = document.querySelector("#form__type");
    const submit = document.querySelector("#popup4 .form__submit .form__btn");

    upLabel.innerText = btn.innerText;
    submit.innerText = btn.innerText;
    type.value = "SPRINTER";
    submit.addEventListener('click', funcDictionary['Add truck'])
}

function setPopup5(btn)
{
    const upLabel = document.querySelector("#popup5 .up__label");
    const name = document.querySelector("#form__load_name");
    const payload = document.querySelector("#form__payload");
    const pickup = document.querySelector("#form__pickup");
    const delivery = document.querySelector("#form__delivery");
    const width = document.querySelector("#form__width");
    const length = document.querySelector("#form__lgh");
    const height = document.querySelector("#form__height");
    const submit = document.querySelector("#popup5 .form__submit .form__btn");

    upLabel.innerText = btn.innerText;
    submit.innerText = btn.innerText;
    name.value = "";
    payload.value = "";
    pickup.value = "";
    delivery.value = "";
    width.value = "";
    length.value = "";
    height.value = "";
    submit.addEventListener('click', funcDictionary['Add load'])
    removeAllSpans();
}

async function setPopup6(btn)
{
    const loadInfo = await getActiveRequest();
    if(!loadInfo)
    {
        btn.setAttribute('href', '#')
        alert('No active loads now');
    }
    else
    {
        btn.setAttribute('href', '#popup6')
        dayjs.extend(relativeTime)
        const id = document.querySelector("#result__id");
        const createdBy = document.querySelector("#result__createdby");
        const assignedTo = document.querySelector("#result__assignedto");
        const status = document.querySelector("#result__status");
        const state = document.querySelector("#result__state");
        const name = document.querySelector("#result__name");
        const payload = document.querySelector("#result__payload");
        const pickup = document.querySelector("#result__pickup");
        const delivery = document.querySelector("#result__delivery");
        const width = document.querySelector("#result__width");
        const length = document.querySelector("#result__length");
        const height = document.querySelector("#result__height");
        const date = document.querySelector("#result__date");
        const nextBtn = document.querySelector('.iterate__next');
        const addBtn = document.querySelector('.add__load');

        id.innerText = loadInfo.load._id;
        createdBy.innerText = loadInfo.load.created_by;
        assignedTo.innerText = loadInfo.load.assigned_to;
        status.innerText = loadInfo.load.status;
        state.innerText = loadInfo.load.state;
        name.innerText = loadInfo.load.name;
        payload.innerText = loadInfo.load.payload;
        pickup.innerText = loadInfo.load.pickup_address;
        delivery.innerText = loadInfo.load.delivery_address;
        width.innerText = loadInfo.load.dimensions.width;
        length.innerText = loadInfo.load.dimensions.length;
        height.innerText = loadInfo.load.dimensions.height;
        date.innerText = dayjs(loadInfo.load.created_date).fromNow();

        const myStorage = window.localStorage;
        myStorage.setItem('load_id', loadInfo.load._id);

        addBtn.style.display = 'none'
        nextBtn.style.display = 'block'
        nextBtn.addEventListener('click', iterateNextRequest)
    }
}

function addLogoutEvent()
{
    const btn = document.querySelector(".logout");
    btn.addEventListener('click', logout)
}

function logout()
{
    const header = document.querySelector(".header");
    const article = document.querySelector(".main__page");
    const authDiv = document.querySelector(".content__auth");

    header.style.display = 'none'
    article.style.display = 'none'
    authDiv.style.display = 'flex'

    const myStorage = window.localStorage;
    myStorage.setItem('jwt', '');

    const btn = document.querySelector("#popup2 .form__btn");
    btn.removeEventListener('click', logout)
}

function addCloseMenuEvent()
{
    const menu = document.querySelector("#bars")
    const page = document.querySelector(".main__page");
    menu.addEventListener('change', function() {
        const menuList = document.querySelector(".menu")
        if(menu.checked)
        {
            menuList.style.display = 'none';
            page.style.gridTemplateColumns = '0fr 4fr';
        }
        else
        {
            menuList.style.display = 'block'
            page.style.gridTemplateColumns = '1fr 3fr';
        }
    })
}

function addRequestEvents()
{
    const btnsWithoutPopup = document.querySelectorAll('.without_btn');
    btnsWithoutPopup.forEach(btn => {
        btn.addEventListener('click', funcDictionary[btn.innerText])
    })
}

module.exports = {
    addDataPopupEvent, 
    addRequestEvents,
    addLogoutEvent,
    addCloseMenuEvent,
    logout,
    setPopup5
};