const _ = require('lodash');
const{getUserLoadsRequest, getLoadPage, addAllRows} = require('../requests/loadRequests')
async function addPaginationEvents()
{
    const firstPageBtn = document.querySelector('.first__page');
    const nextPageBtn = document.querySelector('.next__page');
    const lastPageBtn = document.querySelector('.last__page');
    const myStorage = window.localStorage;
    firstPageBtn.addEventListener('click', async function () {
        const pageLoads = await getLoadPage(0);
        addAllRows(pageLoads.loads);
        myStorage.setItem('load_page', 0)
    });
    lastPageBtn.addEventListener('click', async function () {
        const loads = await getUserLoadsRequest();
        const pageLoads = await getLoadPage(Math.floor((loads.loads.length / 5)));
        addAllRows(pageLoads.loads);
        myStorage.setItem('load_page', Math.floor((loads.loads.length / 5)))
    });
    nextPageBtn.addEventListener('click', async function () {
        const currentPage = myStorage.getItem('load_page');
        const loads = await getUserLoadsRequest();
        if(parseInt(currentPage) < (Math.floor((loads.loads.length / 5))))
        {
            const pageLoads = await getLoadPage(parseInt(currentPage) + 1);
            addAllRows(pageLoads.loads);
            myStorage.setItem('load_page', parseInt(currentPage) + 1)
        }
        else
        {
            const pageLoads = await getLoadPage(Math.floor((loads.loads.length / 5)));
            addAllRows(pageLoads.loads);
            myStorage.setItem('load_page', Math.floor((loads.loads.length / 5)))
        }
    });
}

async function addFilterEvent()
{
    const myStorage = window.localStorage;
    const filterDiv = document.querySelector(".filter__loads");
    filterDiv.addEventListener('change', async function() {

        const filteredLoads = await getLoadPage(0);
        myStorage.setItem('load_page', 0)
        addAllRows(filteredLoads.loads);
    });
};


module.exports = {
    addPaginationEvents,
    addFilterEvent
}