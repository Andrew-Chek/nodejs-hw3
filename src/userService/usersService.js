const { User } = require('../models/Users.js');
const { Credential } = require('../models/Credentials.js');
const { RegistrationCredential } = require('../models/RegistrationCredentials.js')
const bcrypt = require('bcryptjs');
const { Load } = require('../models/Loads.js');
const { Truck } = require('../models/Trucks.js');
const fs = require('fs');
const fspromise = require('fs/promises')

const getUser = async (req, res, next) => {
  const user = await User.findById( req.user._id );
  res.status(200).send({
    user: {
      _id: user._id,
      role: user.role,
      email: user.email,
      created_date: user.created_date,
      img_src: user.img_src
    }
  });
};

const changeUserPassword = async (req, res, next) =>
{
  const newPassword = await bcrypt.hash(req.body.newPassword, 10)
  await RegistrationCredential.findOneAndUpdate({ email: req.user.email }, { $set: { password: newPassword } })
  return Credential.findOneAndUpdate({ email: req.user.email }, { $set: { password: newPassword } })
  .then((result) => {
    res.status(200).json({ message: 'Password changed successfully' });
  });
}

const getUserPhotoBySrc = async (req, res, next) => 
{
  const src = req.body.src;
  fs.readFile(src, function(err, data) {
    if (err) next(err) // Fail if the file can't be read.
    return res.status(200).blob({text: data})
  })
}

const postUserPhoto = async (req, res, next) =>
{
  const image_string = req.body;
  const file = saveImage(image_string);
  return User.findOneAndUpdate({ email: req.user.email }, { $set: { src: `${file.localPath}` } })
  .then(result => {
    res.status(200).json({message: 'Photo posted successfully'})
  })
}

function saveImage(baseImage) {
  console.log(baseImage)
  /*path of the folder where your project is saved. (In my case i got it from config file, root path of project).*/
  const uploadPath = "C:/epam_lab/hw/hw3/nodejs-hw3";
  //path of folder where you want to save the image.
  const localPath = `${uploadPath}/src/files/`;
  //Find extension of file
  const ext = baseImage.substring(baseImage.indexOf("/")+1, baseImage.indexOf(";base64"));
  const fileType = baseImage.substring("data:".length,baseImage.indexOf("/"));
  //Forming regex to extract base64 data of file.
  const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
  //Extract base64 data.
  const base64Data = baseImage.replace(regex, "");
  const rand = Math.ceil(Math.random()*1000);
  //Random photo name with timeStamp so it will not overide previous images.
  const filename = `Photo_${Date.now()}_${rand}.${ext}`;
  
  //Check that if directory is present or not.
  if(!fs.existsSync(`${uploadPath}/uploads/`)) {
      fs.mkdirSync(`${uploadPath}/uploads/`);
  }
  if (!fs.existsSync(localPath)) {
      fs.mkdirSync(localPath);
  }
  fs.writeFileSync(localPath+filename, base64Data, 'base64');
  return {filename, localPath};
}

const deleteUser = async (req, res, next) => 
{
  await RegistrationCredential.findOneAndDelete({ email: req.user.email })
  await Credential.findOneAndDelete({ email: req.user.email })
  if(req.user.role == 'SHIPPER')
  {
    await Load.findOneAndDelete({created_by: req.user._id})
  }
  else
  {
    await Load.findOneAndDelete({assigned_to: req.user._id})
    await Truck.findOneAndDelete({created_by: req.user._id})
  }
  return User.findByIdAndDelete(req.user._id)
  .then(res.status(200).json({ message: "Profile deleted successfully"}))
}

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
  getUserPhotoBySrc,
  postUserPhoto
};
