const express = require('express');
const router = express.Router();
const {
  getMyLoads, 
  createLoad, 
  getActiveLoad,
  iterateNextLoadState,
  getMyLoadById, 
  updateMyLoadById, 
  deleteMyLoadById,
  postLoad,
  getLoadShippingInfo
} = require('./loadsService.js');
const { authMiddleware } = require('../middleware/authMiddleware');
const { checkDriverRole, checkAssignedDriver } = require('../middleware/truckMiddlewares')
const { checkShipperRole, checkLoad, checkExistingLoad, checkExistingActiveLoad } = require('../middleware/loadMiddlewares');

router.use(authMiddleware)

router.get('/', getMyLoads);

router.post('/', checkShipperRole, checkLoad, createLoad);

router.get('/active', checkDriverRole, checkExistingActiveLoad, getActiveLoad);

router.patch('/active/state', checkDriverRole, checkExistingActiveLoad, iterateNextLoadState);

router.get('/:id', checkExistingLoad, getMyLoadById);

router.put('/:id', checkShipperRole, checkExistingLoad, checkLoad, updateMyLoadById);

router.delete('/:id', checkShipperRole, checkExistingLoad, deleteMyLoadById);

router.post('/:id/post', checkShipperRole, checkExistingLoad, checkAssignedDriver, postLoad);

router.get('/:id/shipping_info', checkShipperRole, checkExistingLoad, getLoadShippingInfo);

module.exports = {
  loadsRouter: router,
};