const { Load } = require('../models/Loads.js');
const { Truck } = require('../models/Trucks.js');

const states = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];

const getMyLoads = (req, res, next) => {
  const limit = checkLimit(req, res, next);
  const offset = checkOffset(req, res, next);
  if(req.user.role == 'DRIVER')
  {
    if(checkStatus(req, res, next))
    {
      return Load.find({assigned_to: req.user._id, status: req.query.status}, '-__v').skip(offset).limit(limit).then((result) => {
        res.status(200).json({
          "loads": result
        });
      });
    }
    else
    {
      return Load.aggregate([ 
        {
          '$match': {
            'assigned_to': req.user._id
          }
        },
        {
          '$skip': offset
        }, {
          '$limit': limit
        }
      ]).then((result) => {
        res.status(200).json({
          "loads": result
        })
      });
    }
  }
  else
  {
    if(checkStatus(req, res, next))
    {
      return Load.find({created_by: req.user._id, status: req.query.status}, '-__v').skip(offset).limit(limit).then((result) => {
        res.status(200).json({
          "loads": result
        });
      });
    }
    else
    {
      return Load.find({created_by: req.user._id}, '-__v').skip(offset).limit(limit).then((result) => {
        res.status(200).json({
          "loads": result
        });
      });
    }
  }
}

const checkLimit = (req, res, next) => {
  if(req.query.limit != undefined && !isNaN(parseInt(req.query.limit)))
  {
    if(parseInt(req.query.limit) > 0 && parseInt(req.query.limit) < 50)
    {
      return parseInt(req.query.limit)
    }
  }
  else return 10;
}

const checkOffset = (req, res, next) => {
  if(req.query.offset != undefined && !isNaN(parseInt(req.query.offset)) && parseInt(req.query.offset) > 0)
  {
    return parseInt(req.query.offset)
  }
  else return 0;
}

const checkStatus = (req, res, next) => {
  if(req.query.status != undefined && req.query.status != '')
  {
    return true;
  }
  else return false;
}

function createLoad(req, res, next) {
  const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
  const created_date = new Date().toISOString();
  const load = new Load({
    created_by: req.user._id,
    status: 'NEW',
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_date
  });
  load.save().then(
    res.status(200).json({message: "Load created successfully"})
  );
}

const getActiveLoad = async (req, res, next) => {
  return Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'})
  .then((load) => {
    res.status(200).json({
      "load": load});
  });
}

const iterateNextLoadState = async (req, res, next) => {
  const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'});
  console.log(load)
  const nextState = getNextState(load.state);
  if(nextState == 'Arrived to delivery')
  {
    const time = new Date().toISOString();
    const message = `Load shipped to delivery address`;
    await Load.findOneAndUpdate({assigned_to: req.user._id, status: 'ASSIGNED'}, { $set: { status: 'SHIPPED'} })
    await Load.findOneAndUpdate({_id: req.params.id, created_by: req.user._id}, { $push: {logs: {message, time} } })
    await Truck.findOneAndUpdate({assigned_to: req.user._id}, { $set: { status: 'IS', assigned_to: null } })
    return Load.findOneAndUpdate({assigned_to: req.user._id, status: 'ASSIGNED'}, { $set: { state: nextState } })
    .then((load) => {
      res.status(200).json({
        "message": `Load state changed to '${nextState}'`});
    });
  }
  return Load.findOneAndUpdate({assigned_to: req.user._id, status: 'ASSIGNED'}, { $set: { state: nextState } })
  .then((load) => {
    res.status(200).json({
      "message": `Load state changed to '${nextState}'`});
  });
}

const getNextState = (currentState) =>
{
  for(let i = 0; i < states.length; i++)
  {
    if(states[ i ] == currentState)
    {
      return states[ i + 1 ];
    }
  }
}

const getMyLoadById = async (req, res, next) => {
  if(req.user.role == 'Shipper')
  {
    return Load.findById({_id: req.params.id, created_by: req.user._id})
    .then((load) => {
      res.status(200).json({
        "load": load});
    });
  }
  else
  {
    return Load.findById({_id: req.params.id, assigned_to: req.user._id})
    .then((load) => {
      res.status(200).json({
        "load": load});
    });
  }
}

const updateMyLoadById = async (req, res, next) => {
  const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
  return Load.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id}, {$set: { name, payload, pickup_address, delivery_address, dimensions } })
    .then((result) => {
      res.status(200).json({message: 'Load details changed successfully'});
    });
}

const deleteMyLoadById = (req, res, next) => {
  Load.findByIdAndDelete({_id: req.params.id, created_by: req.user._id})
  .then((result) => {
    res.status(200).json({message: 'Load deleted successfully'});
  });
}

const postLoad = async (req, res, next) => {
  const load = await Load.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id}, { $set: {status: 'POSTED' } } );
  const found = await searchForDriver(req, res, next, load);
  return res.status(200).json({message: 'Load posted successfully', driver_found: found});
}

const searchForDriver = async (req, res, next, load) => {
  const trucks = await Truck.aggregate([
    {
      '$match': {
        'assigned_to': {
          '$ne': null
        }
      }
    }, {
      '$match': {
        'payload': {
          '$gt': load.payload
        }
      }
    }, {
      '$unwind': '$dimensions'
    }, {
      '$match': {
        'dimensions.width': {
          '$gt': load.dimensions.width
        }
      }
    }, {
      '$match': {
        'dimensions.length': {
          '$gt': load.dimensions.length
        }
      }
    }, {
      '$match': {
        'dimensions.height': {
          '$gt': load.dimensions.height
        }
      }
    }, {
      '$match': {
        'status': 'IS'
      }
    }
  ]);
  if(trucks != [])
  {
    const time = new Date().toISOString();
    const message = `Load assigned to driver with id ${trucks[0].assigned_to}`;
    await Truck.findOneAndUpdate({assigned_to: trucks[0].assigned_to}, { $set: { status: 'OL' } })
    await Load.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id}, 
      { $set: {status: 'ASSIGNED', assigned_to: trucks[0].assigned_to, state: 'En route to Pick Up' } });
    await Load.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id},
      { $push: {logs: {message, time}} })
    return true;
  }
  else
  {
    await Load.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id}, { $set: {status: 'NEW' } } );
    return false;
  }
}

const getLoadShippingInfo = async (req, res, next) => {
  const load = await Load.findOne({_id: req.params.id, created_by: req.user._id, status: 'ASSIGNED'});
  const truck = await Truck.findOne({assigned_to: load.assignd_to});
  return res.json({load, truck});
}

module.exports = {
  getMyLoads,
  createLoad,
  getActiveLoad,
  iterateNextLoadState,
  getMyLoadById,
  updateMyLoadById,
  deleteMyLoadById,
  postLoad,
  getLoadShippingInfo
};