const mongoose = require('mongoose');

const User = mongoose.model('User', {
  role: {
    type: String,
    required: true,
    enum: ['DRIVER','SHIPPER']
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  created_date: {
    type: String,
    required: true,
  },
  img_src: {
    type: String,
    required: false,
  }
});

module.exports = {
  User,
};
