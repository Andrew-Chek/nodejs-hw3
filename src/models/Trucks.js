const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
  },
  payload: {
    type: Number,
    required: true,
    enum: [1700, 2500, 4000]
  },
  dimensions: {
    type: {
        width: Number, 
        length: Number, 
        height: Number
    },
    required: true,
  },
  status: {
    type: String,
    required: true,
    enum: ['OL', 'IS']
  },
  created_date: {
    type: String,
    required: true,
  }
});

module.exports = {
  Truck,
};