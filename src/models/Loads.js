const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']
  },
  state: {
    type: String,
    required: false,
    enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {
        width: Number, 
        length: Number, 
        height: Number
    },
    required: true,
  },
  logs: {
    type: [
        {
            message: String,
            time: String
        }
    ],
    required: false,
  },
  created_date: {
    type: String,
    required: true,
  }
});

module.exports = {
    Load,
};