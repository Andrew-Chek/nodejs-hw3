const mongoose = require('mongoose');

const RegistrationCredential = mongoose.model('registrationCredential', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ['DRIVER','SHIPPER']
  },
});

module.exports = {
    RegistrationCredential,
};