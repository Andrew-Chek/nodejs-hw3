const { Load } = require('../models/Loads.js');

const checkShipperRole = (req, res, next) => {
    if(req.user.role != 'SHIPPER')
    {
        next(new Error('This action is available only for shippers'))
        return;
    }
    next()
}

const checkLoad = (req, res, next) => {
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    if(name == "")
    {
        next(new Error(`Name of load cannot be empty`));
        return;
    }
    if(payload == "" || isNaN(parseInt(payload)))
    {
        next(new Error(`Payload isn't correct`));
        return;
    }
    if(pickup_address == "")
    {
        next(new Error(`Pickup address cannot be empty`));
        return;
    }
    if(delivery_address == "")
    {
        next(new Error(`Delivery address cannot be empty`));
        return;
    }
    if(dimensions.width == "" || dimensions.length == "" || dimensions.height == "")
    {
        next(new Error(`One of dimension params is empty`));
        return;
    }
    if(isNaN(parseInt(dimensions.width)) || isNaN(parseInt(dimensions.length)) || isNaN(parseInt(dimensions.height)))
    {
        next(new Error(`One of dimension params is not a number`));
        return;
    }
    next();
}

const checkExistingLoad = async (req, res, next) =>
{
  try{
    const load = await Load.findById(req.params.id);
    if(load == null)
    {
      next(new Error(`Load wasn't found`))
      return;
    }
    next();
  }
  catch(err)
  {
    return res.status(500).json({ message: `Id format isn't correct to find in data base`})
  }
}

const checkExistingActiveLoad = async (req, res, next) => 
{
    const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'})
    if(load == null)
    {
      next(new Error(`Load wasn't found`))
      return;
    }
    next();
}

module.exports = {
    checkShipperRole, 
    checkLoad,
    checkExistingLoad,
    checkExistingActiveLoad,
}