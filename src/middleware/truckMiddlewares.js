const { Truck } = require('../models/Trucks.js');
const { Load } = require('../models/Loads.js');

const checkDriverRole = async (req, res, next) => {
    if(req.user.role != 'DRIVER')
    {
        next(new Error('This action is available only for drivers'))
        return;
    }
    next()
}

const checkExistingTruck = async (req, res, next) =>
{
  try{
    const truck = await Truck.findById(req.params.id);
    if(truck == null)
    {
      next(new Error(`Truck wasn't found`))
      return;
    }
    next();
  }
  catch(err)
  {
    return res.status(500).json({ message: `Id format isn't correct to find in data base`})
  }
}

const checkTruckType = (req, res, next) => {
  const { type } = req.body;
  if(type == '')
  {
    next(new Error('Type of truck cannot be empty'))
    return;
  }
  if(type != 'SPRINTER' && type != 'SMALL STRAIGHT' && type != 'LARGE STRAIGHT')
  {
    next(new Error(`Such type of truck doesn't exist`))
    return;
  }
  next();
}

const checkAssignedTruck = async (req, res, next) => {
  const truck = await Truck.findOne({ assigned_to: req.user._id });
  if(truck != null)
  {
    next(new Error(`This user already has a truck`))
    return;
  }
  next();
}

const checkExistingActiveLoad = async (req, res, next) => 
{
    const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'})
    if(load != null)
    {
      next(new Error(`Driver cannot change his info on load`))
      return;
    }
    next();
}

const checkAssignedDriver = async (req, res, next) => 
{
  const truck = await Truck.findOne({ assigned_to: { $ne: null } });
  if(truck == null)
  {
    next(new Error(`No available trucks for posting a load`))
    return;
  }
  next();
}

module.exports = {
    checkDriverRole,
    checkExistingTruck,
    checkTruckType,
    checkAssignedTruck,
    checkExistingActiveLoad,
    checkAssignedDriver
}