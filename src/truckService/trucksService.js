const { Truck } = require('../models/Trucks.js');
const trucks = {
  'SPRINTER': {payload: 1700, dimensions: {width: 300, length: 250, height: 170}}, 
  'SMALL STRAIGHT': { payload: 2500, dimensions: {width: 500, length: 250, height: 170}},
  'LARGE STRAIGHT': { payload: 4000, dimensions: {width: 700, length: 350, height: 200}}
  };

const getMyTrucks = (req, res, next) => {
  return Truck.find({created_by: req.user._id}, '-__v').then((result) => {
    res.status(200).json({
      "trucks": result
    });
  });
}

function createTruck(req, res, next) {
  const { type } = req.body;
  const created_date = new Date().toISOString();
  const truckParams = trucks[type];
  const truck = new Truck({
    created_by: req.user._id,
    type,
    payload: truckParams.payload,
    dimensions: truckParams.dimensions,
    status: 'IS',
    created_date,
  });
  truck.save().then(
    res.status(200).json({message: "Truck created successfully"})
  );
}

const getMyTruckById = async (req, res, next) => {
  return Truck.findById({_id: req.params.id, created_by: req.user._id})
    .then((truck) => {
      res.status(200).json({
        "truck": {
          _id: truck._id, 
          created_by: truck.created_by, 
          assigned_to: truck.assigned_to, 
          type: truck.type, 
          status: truck.status,
          created_date: truck.created_date
        }});
    });
}

const updateMyTruckById = async (req, res, next) => {
  const { type } = req.body;
  const truckParams = trucks[type];
  return Truck.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id}, 
    {$set: { type, payload: truckParams.payload, dimensions: truckParams.dimensions } })
    .then((result) => {
      res.json({message: 'Truck details changed successfully'});
    });
}

const deleteMyTruckById = (req, res, next) => {
  Truck.findByIdAndDelete({_id: req.params.id, created_by: req.user._id})
  .then((result) => {
    res.json({message: 'Truck deleted successfully'});
  });
}

const assignMyTruckById = async (req, res, next) => {
  return Truck.findByIdAndUpdate({_id: req.params.id, created_by: req.user._id}, {$set: { assigned_to: req.user._id } })
  .then((result) => {
    res.json({message: 'Truck assigned successfully'});
  });
}

module.exports = {
  getMyTrucks,
  createTruck,
  getMyTruckById,
  updateMyTruckById,
  deleteMyTruckById,
  assignMyTruckById
};
