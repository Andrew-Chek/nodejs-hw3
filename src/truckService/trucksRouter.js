const express = require('express');
const router = express.Router();
const {
  getMyTrucks, createTruck, getMyTruckById, updateMyTruckById, deleteMyTruckById, assignMyTruckById
} = require('./trucksService.js');
const { authMiddleware } = require('../middleware/authMiddleware');
const { 
  checkDriverRole, 
  checkExistingTruck, 
  checkTruckType, 
  checkAssignedTruck, 
  checkExistingActiveLoad
} = require('../middleware/truckMiddlewares');

router.use(authMiddleware, checkDriverRole, checkExistingActiveLoad)

router.post('/', checkTruckType, createTruck);

router.get('/', getMyTrucks);

router.get('/:id', checkExistingTruck, getMyTruckById);

router.put('/:id', checkExistingTruck, checkTruckType, updateMyTruckById);

router.delete('/:id', checkExistingTruck, deleteMyTruckById);

router.post('/:id/assign', checkExistingTruck, checkAssignedTruck, assignMyTruckById);

module.exports = {
  trucksRouter: router,
};
